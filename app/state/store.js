import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import rootReducer from './reducers';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [ sagaMiddleware ];
let storeEnhancer;
// eslint-disable-next-line
if (__DEV__) {
	const { createLogger } = require('redux-logger');
	// eslint-disable-next-line
	const logger = createLogger({
		collapsed: true
	});
	// middlewares.push(logger);
	const composeDevToolsEnhancers = composeWithDevTools({});
	storeEnhancer = composeDevToolsEnhancers(applyMiddleware(...middlewares));
} else {
	storeEnhancer = applyMiddleware(...middlewares);
}
const persistConfig = {
	key: 'root',
	storage: AsyncStorage
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, storeEnhancer);

sagaMiddleware.run(rootSaga);

const persistor = persistStore(store);
export { store, persistor };
