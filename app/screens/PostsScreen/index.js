import React from 'react';
import { View, FlatList, StyleSheet, Text, ListView } from 'react-native';
import PropTypes from 'prop-types';
import { Button, Icon, Container, Fab, SwipeRow, Content, List, ListItem, Left, Body, Right } from 'native-base';
import enhancer from './enhancer';
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
const PostsScreenView = ({ posts, onDeleteAllPress, fetching, fetchFailed, onDeleteItemPress, onItemPress }) => {
	const deleteRow = (secId, rowId, rowMap) => {
		rowMap[`${secId}${rowId}`].props.closeRow();
		const item = posts[rowId];
		onDeleteItemPress(item.id);
	};

	return (
		<Container style={{ paddingStart: 20 }}>
			{(fetching || posts.length === 0) && (
				<View style={{ flex: 1, alignContent: 'center', justifyContent: 'center' }}>
					<Text style={{ fontFamily: 'OpenSans-Regular', textAlign: 'center' }}>
						{fetching ? 'Loading items ...' : 'No items found'}
					</Text>
				</View>
			)}
			{posts.length > 0 && (
				<Content>
					<List
						leftOpenValue={0}
						rightOpenValue={-75}
						dataSource={ds.cloneWithRows(posts)}
						renderRow={(data) => (
							<ListItem
								icon
								onPress={() => {
									onItemPress(data.id);
								}}
							>
								<Left>
									{!data.isFavorite ? (
										<Icon
											active
											name="radio-button-on"
											style={{
												color: 'blue',
												fontSize: 16,
												opacity: Number(!!data.isNew)
											}}
										/>
									) : (
										<Icon
											active
											name="star"
											style={{
												color: 'orange',
												fontSize: 16
											}}
										/>
									)}
								</Left>
								<Body>
									<Text style={{ fontFamily: 'OpenSans-Regular' }}>
										{data.id} {data.title}
									</Text>
								</Body>
								<Right>
									<Icon active name="arrow-forward" />
								</Right>
							</ListItem>
						)}
						renderRightHiddenRow={(data, secId, rowId, rowMap) => (
							<Button full danger onPress={(_) => deleteRow(secId, rowId, rowMap)}>
								<Icon active name="trash" />
							</Button>
						)}
					/>
				</Content>
			)}
			{/* <FlatList
				data={posts}
				renderItem={({ item }) => (
					<SwipeRow
						leftOpenValue={0}
						rightOpenValue={-75}
						body={
							<View style={{ flexDirection: 'row' }}>
								<View style={{ flex: 0.15 }}>
									<Icon active name="trash" />
								</View>
								<View style={{ flex: 0.85 }}>
									<Text>{item.title}</Text>
								</View>
							</View>
						}
						right={
							<Button danger onPress={() => alert('Trash')}>
								<Icon active name="trash" />
							</Button>
						}
					/>
				)}
				keyExtractor={(item, index) => `${item.id}`}
				ListEmptyComponent={() => <Text>{fetching ? 'Cargando' : 'No hay items'}</Text>}
				ItemSeparatorComponent={() => (
					<View style={{ height: StyleSheet.hairlineWidth, backgroundColor: 'red' }} />
				)}
            />  */}
			<Fab style={{ backgroundColor: 'red' }} position="bottomRight" onPress={onDeleteAllPress}>
				<Icon name="trash" />
			</Fab>
		</Container>
	);
};
PostsScreenView.propTypes = {
	posts: PropTypes.array.isRequired,
	onDeleteAllPress: PropTypes.func,
	fetching: PropTypes.bool,
	fetchFailed: PropTypes.string,
	onItemPress: PropTypes.func.isRequired
};
PostsScreenView.defaultProps = {
	onDeleteAllPress: () => {},
	fetching: false,
	fetchFailed: ''
};
const PostsScreen = enhancer(PostsScreenView);
export default PostsScreen;
