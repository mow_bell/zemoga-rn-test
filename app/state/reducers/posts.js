import {
	FETCH_POSTS,
	FETCH_POSTS_SUCCESS,
	FETCH_POSTS_FAILED,
	CLEAR_POSTS,
	RELOAD_POSTS,
	DELETE_POST,
	POST_OPENED,
	TOGGLE_POST_AS_FAVORITE,
	SET_POST_EXTRA_INFO
} from '../actions/types/posts';
import clone from 'clone';

const initialState = {
	fetching: false,
	fetchFailed: '',
	posts: [],
	lastPostIDOpen: null
};
const postsReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_POSTS:
			return {
				...state,
				fetching: true,
				fetchFailed: ''
			};
		case FETCH_POSTS_SUCCESS:
			return {
				...state,
				fetching: false,
				fetchFailed: '',
				posts: action.posts
			};
		case FETCH_POSTS_FAILED:
			return {
				...state,
				fetching: false,
				fetchFailed: action.error,
				posts: []
			};
		case CLEAR_POSTS:
		case RELOAD_POSTS:
			return {
				...initialState
			};
		case DELETE_POST: {
			let curPosts = clone(state.posts);
			const postIndex = curPosts.findIndex((post) => {
				return post.id === action.id;
			});
			if (postIndex !== -1) {
				curPosts = [ ...curPosts.slice(0, postIndex), ...curPosts.slice(postIndex + 1) ];
			}
			return {
				...state,
				posts: curPosts
			};
		}
		case POST_OPENED: {
			const curPosts = clone(state.posts);
			const post = curPosts.find((post) => {
				return post.id === action.id;
			});
			if (post) post.isNew = false;
			return {
				...state,
				posts: curPosts,
				lastPostIDOpen: action.id
			};
		}
		case TOGGLE_POST_AS_FAVORITE: {
			const curPosts = clone(state.posts);
			const post = curPosts.find((post) => {
				return post.id === action.id;
			});
			if (post) post.isFavorite = !post.isFavorite;
			return {
				...state,
				posts: curPosts
			};
		}
		case SET_POST_EXTRA_INFO: {
			const curPosts = clone(state.posts);
			const post = curPosts.find((post) => {
				return post.id === action.id;
			});
			if (post) {
				post.user = action.user;
				post.comments = [ ...action.comments.slice(0, 3) ];
			}
			return {
				...state,
				posts: curPosts
			};
		}
		default:
			return state;
	}
};

export default postsReducer;
