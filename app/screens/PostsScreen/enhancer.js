import { compose, withHandlers, lifecycle } from 'recompose';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { fetchPostsOnStartup, clearPosts, reloadPosts, deletePost } from '../../state/actions/posts';

function mapStateToProps(state) {
	const { posts } = state;
	return {
		...posts
	};
}
function mapDispatchToProps(dispatch) {
	return bindActionCreators(
		{
			fetchPostsOnStartup,
			clearPosts,
			reloadPosts,
			deletePost
		},
		dispatch
	);
}

const screenHandlers = withHandlers({
	onDeleteAllPress: ({ clearPosts }) => () => {
		clearPosts();
	},
	onDeleteItemPress: ({ deletePost }) => (id) => {
		deletePost(id);
	},
	onReloadPostsPress: ({ reloadPosts }) => () => {
		reloadPosts();
	},
	onItemPress: ({ navigation }) => (id) => {
		navigation.navigate('Details', { id });
	}
});

const enhancer = compose(
	withNavigation,
	connect(mapStateToProps, mapDispatchToProps),
	screenHandlers,
	lifecycle({
		componentDidMount() {
			this.props.fetchPostsOnStartup();
			this.props.navigation.setParams({ reloadPosts: this.props.onReloadPostsPress });
		}
	})
);
export default enhancer;
