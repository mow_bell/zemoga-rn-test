import { all, takeEvery, select, call, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import { FETCH_POSTS_ON_STARTUP, RELOAD_POSTS, POST_OPENED } from '../actions/types/posts';
import { fetchPosts, fetchPostsSuccess, fetchPostsFailed, setPostExtraInfo } from '../actions/posts';

const axiosInstance = axios.create({
	baseURL: 'https://jsonplaceholder.typicode.com'
});
const MAX_COUNT_OF_NEW_ITEMS = 20;
function* requestFetchPosts() {
	const response = yield call(axiosInstance.get, '/posts');
	return response.data;
}

function* fetchAllPosts(action) {
	try {
		const { posts: currentPosts } = yield select((state) => state.posts);
		if (currentPosts.length > 0) return;
		yield put(fetchPosts());
		const posts = yield call(requestFetchPosts);
		const postsMetadata = posts.map((post, index) => {
			return {
				...post,
				isFavorite: false,
				isNew: index <= MAX_COUNT_OF_NEW_ITEMS,
				user: null,
				comments: []
			};
		});
		yield put(fetchPostsSuccess(postsMetadata));
	} catch (error) {
		yield put(fetchPostsFailed(error.message));
	}
}

function* requestUser(id) {
	const response = yield call(axiosInstance.get, `/users/${id}`);
	return response.data;
}

function* requestPostComments(id) {
	const response = yield call(axiosInstance.get, `/posts/${id}/comments`);
	return response.data;
}

function* fetchPostExtraInfo({ id: postId }) {
	try {
		debugger;
		const { posts } = yield select((state) => state.posts);
		const post = posts.find((post) => {
			return post.id == postId;
		});
		debugger;
		if (!post) return;
		if (post.user && post.comments && post.comments.length > 0) return;
		const user = yield call(requestUser, post.userId);
		const comments = yield call(requestPostComments, postId);
		debugger;
		yield put(setPostExtraInfo(postId, user, comments));
	} catch (error) {}
}

function* rootSaga() {
	try {
		yield all([ takeEvery(FETCH_POSTS_ON_STARTUP, fetchPosts) ]);
		yield all([ takeLatest(RELOAD_POSTS, fetchAllPosts) ]);
		yield all([ takeLatest(POST_OPENED, fetchPostExtraInfo) ]);
	} catch (e) {
		// handle fetchAll errors
	}
}
export default rootSaga;
