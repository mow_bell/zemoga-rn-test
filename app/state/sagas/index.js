import { spawn, all } from 'redux-saga/effects';

import postsSaga from './posts';

function* rootSaga() {
	try {
		yield all([ spawn(postsSaga) ]);
	} catch (e) {
		// handle uncaught
	}
}

export default rootSaga;
