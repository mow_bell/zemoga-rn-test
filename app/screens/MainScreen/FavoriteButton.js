import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Button, Icon } from 'native-base';
import { togglePostAsFavorite } from '../../state/actions/posts';
const FavoriteButtonView = ({ postId, togglePostAsFavorite, isFavoritePost }) => {
	return (
		<Button
			transparent
			onPress={() => {
				togglePostAsFavorite(postId);
			}}
		>
			<Icon name={!isFavoritePost ? 'star-outline' : 'star'} style={{ color: 'white' }} />
		</Button>
	);
};
function mapStateToProps(state, props) {
	const { posts: { posts, lastPostIDOpen } } = state;
	const post = posts.find((post) => {
		return post.id === lastPostIDOpen;
	});
	const isFavoritePost = post && post.isFavorite;
	debugger;
	return {
		isFavoritePost,
		postId: lastPostIDOpen
	};
}
function mapDispatchToProps(dispatch) {
	return bindActionCreators(
		{
			togglePostAsFavorite
		},
		dispatch
	);
}
const enhancer = connect(mapStateToProps, mapDispatchToProps);
const FavoriteButton = enhancer(FavoriteButtonView);
export default FavoriteButton;
