import {
	FETCH_POSTS_ON_STARTUP,
	FETCH_POSTS,
	FETCH_POSTS_SUCCESS,
	FETCH_POSTS_FAILED,
	CLEAR_POSTS,
	RELOAD_POSTS,
	POST_OPENED,
	DELETE_POST,
	TOGGLE_POST_AS_FAVORITE,
	SET_POST_EXTRA_INFO
} from './types/posts';

export const fetchPostsOnStartup = () => ({
	type: FETCH_POSTS_ON_STARTUP
});

export const fetchPosts = () => ({
	type: FETCH_POSTS
});

export const fetchPostsSuccess = (posts = []) => ({
	type: FETCH_POSTS_SUCCESS,
	posts
});

export const fetchPostsFailed = (error) => ({
	type: FETCH_POSTS_FAILED,
	error
});

export const clearPosts = () => ({
	type: CLEAR_POSTS
});

export const reloadPosts = () => ({
	type: RELOAD_POSTS
});

export const deletePost = (id) => ({
	type: DELETE_POST,
	id
});

export const postOpened = (id) => ({
	type: POST_OPENED,
	id
});

export const togglePostAsFavorite = (id) => ({
	type: TOGGLE_POST_AS_FAVORITE,
	id
});
export const setPostExtraInfo = (id, user, comments) => ({
	type: SET_POST_EXTRA_INFO,
	id,
	user,
	comments
});
