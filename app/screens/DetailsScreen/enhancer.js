import { compose, withHandlers, lifecycle } from 'recompose';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { postOpened } from '../../state/actions/posts';

function mapStateToProps(state, props) {
	const { navigation: { state: { params } } } = props;
	let post = null;
	let postId = null;
	if (params && params.id) {
		postId = params.id;
		const { posts: { posts } } = state;
		post = posts.find((post) => {
			return post.id === postId;
		});
	}
	return {
		post,
		postId
	};
}
function mapDispatchToProps(dispatch) {
	return bindActionCreators(
		{
			postOpened
		},
		dispatch
	);
}

const screenHandlers = withHandlers({
	markPostAsRead: ({ postOpened, postId }) => () => {
		postOpened(postId);
	}
});

const enhancer = compose(
	connect(mapStateToProps, mapDispatchToProps),
	screenHandlers,
	lifecycle({
		componentWillMount() {
			const { markPostAsRead } = this.props;
			markPostAsRead();
		}
	})
);
export default enhancer;
