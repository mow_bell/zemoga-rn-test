import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './state/store';
import MainScreen from './screens/MainScreen';

export default () => (
	<Provider store={store}>
		<PersistGate loading={null} persistor={persistor}>
			<MainScreen />
		</PersistGate>
	</Provider>
);
