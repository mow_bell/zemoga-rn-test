import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { Button, Icon, Text, Container, Content, H3, List, ListItem } from 'native-base';
import enhancer from './enhancer';

const DetailsScreenView = ({ postId, post }) => {
	const { body: postBody } = post;
	return (
		<Container style={{ padding: 20 }}>
			<Content>
				<H3 style={{ fontFamily: 'OpenSans-Bold', paddingBottom: 10 }}>Description</H3>
				<Text style={{ fontFamily: 'OpenSans-Regular', paddingBottom: 10 }}>{postBody}</Text>
				{post.user && (
					<Fragment>
						<H3 style={{ fontFamily: 'OpenSans-Bold', paddingBottom: 10, paddingTop: 10 }}>User</H3>
						<Text style={{ fontFamily: 'OpenSans-Regular' }}>{`Name: ${post.user.name}`}</Text>
						<Text style={{ fontFamily: 'OpenSans-Regular' }}>{`Email: ${post.user.email}`}</Text>
						<Text style={{ fontFamily: 'OpenSans-Regular' }}>{`Phone: ${post.user.phone}`}</Text>
						<Text style={{ fontFamily: 'OpenSans-Regular' }}>{`Website: ${post.user.website}`}</Text>
					</Fragment>
				)}
				{post.comments.length > 0 && (
					<Fragment>
						<H3 style={{ fontFamily: 'OpenSans-Bold', paddingBottom: 10, paddingTop: 20 }}>Comments</H3>
						<List>
							{post.comments.map(({ id, body }) => {
								return (
									<ListItem key={`comment-${id}`}>
										<Text style={{ fontFamily: 'OpenSans-Regular', paddingBottom: 5 }}>{body}</Text>
									</ListItem>
								);
							})}
						</List>
					</Fragment>
				)}
			</Content>
		</Container>
	);
};
DetailsScreenView.propTypes = {
	postId: PropTypes.number,
	post: PropTypes.shape({
		id: PropTypes.number.isRequired,
		title: PropTypes.string.isRequired,
		body: PropTypes.string.isRequired,
		user: PropTypes.object,
		comments: PropTypes.array.isRequired
	})
};
DetailsScreenView.defaultProps = {
	postId: null
};
const DetailsScreen = enhancer(DetailsScreenView);
export default DetailsScreen;
