import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Button, Icon, Container } from 'native-base';
import PostsScreen from '../PostsScreen';
import DetailsScreen from '../DetailsScreen';
import FavoriteButton from './FavoriteButton';

const MainNavigator = createStackNavigator(
	{
		Home: {
			screen: PostsScreen,
			navigationOptions: ({ navigation }) => ({
				title: 'Posts',
				headerRight: (
					<Button
						transparent
						onPress={() => {
							const { state: { params: { reloadPosts } } } = navigation;
							reloadPosts && reloadPosts();
						}}
					>
						<Icon name="refresh" style={{ color: 'white' }} />
					</Button>
				),
				headerBackTitle: null
			})
		},
		Details: {
			screen: DetailsScreen,
			navigationOptions: ({ navigation }) => {
				return {
					headerRight: <FavoriteButton />
				};
			}
		}
	},
	{
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: 'green'
			},
			headerTitleStyle: {
				fontFamily: 'OpenSans-Regular',
				color: 'white'
			},
			headerBackTitleStyle: {
				color: 'white'
			},
			headerTintColor: 'white'
		}
	}
);

const MainScreen = createAppContainer(MainNavigator);
export default MainScreen;
