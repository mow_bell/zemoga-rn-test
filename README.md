# Zemoga Fornt End Test

By Erwing Mauricio Bello

---

## React Native Setup

> **MANDATORY** Install React Native for Build Projects with Native code (No Expo based projects)

> Follow steps in [React Native Getting Started Guide](https://facebook.github.io/react-native/docs/getting-started.html) (choose _Building Projects with Native Code_)

## Installation

- 1.  Locate root folder, run `yarn`
- 2.  Run android emulator or ios simulator (or devices) follow commands explained [here](#commands-to-run-app)
`react-native run-ios` or `react-native run-android`

---

## Required tools

| |Version |
|-------------------------------------------|---------|
|Node | 8.9.3 |
|npm | 5.5.1 |
|[Yarn](https://yarnpkg.com/en/) | 1.5.1 |
|[Yarn](https://yarnpkg.com/en/) | 1.5.1 |
|[Native Base](https://nativebase.io) | 2.8.0 |
|[React Navigation](https://reactnavigation.org) | 1.5.12 |
---

## Choosen frameworks

### Native Base
I used Native Base for UI components because it's a robust framework, with several common styled components

### React Navigation
It's a well known library for navigation in React Native, suportting absence of a core navigation RN library

### Redux
An state manager that provides a known and tested methodology/framework to handle aplication state

### Redux Saga
Tool to handle redux async processes
